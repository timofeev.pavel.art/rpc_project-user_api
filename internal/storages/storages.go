package storages

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/db/adapter"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/cache"
	ustorage "gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
