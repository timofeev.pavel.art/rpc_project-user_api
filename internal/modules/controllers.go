package modules

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/component"
	ucontroller "gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/modules/user/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}
