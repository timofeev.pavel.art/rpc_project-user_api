package service

import (
	"context"

	"github.com/lib/pq"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/user"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/errors"
	models2 "gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/modules/user/storage"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	return &UserService{storage: storage, logger: logger}
}

func (u *UserService) Create(ctx context.Context, in user.UserCreateIn) user.UserCreateOut {
	var dto models2.UserDTO
	dto.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, dto)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return user.UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return user.UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return user.UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) Update(ctx context.Context, in user.UserUpdateIn) user.UserUpdateOut {
	panic("implement me")
}

func (u *UserService) VerifyEmail(ctx context.Context, in user.UserVerifyEmailIn) user.UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return user.UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return user.UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return user.UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in user.ChangePasswordIn) user.ChangePasswordOut {
	panic("implement me")
}

func (u *UserService) GetByEmail(ctx context.Context, in user.GetByEmailIn) user.UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return user.UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return user.UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByPhone(ctx context.Context, in user.GetByPhoneIn) user.UserOut {
	panic("implement me")
}

func (u *UserService) GetByID(ctx context.Context, in user.GetByIDIn) user.UserOut {
	userDTO, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return user.UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return user.UserOut{
		User: &models.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.Role,
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByIDs(ctx context.Context, in user.GetByIDsIn) user.UsersOut {
	panic("implement me")
}
