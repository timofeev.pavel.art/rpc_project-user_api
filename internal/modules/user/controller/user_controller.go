package controller

import (
	"net/http"

	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/message/user_mess"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/user"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/component"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/handlers"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/modules/user/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), user.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, user_mess.ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: user_mess.Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, user_mess.ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: user_mess.Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
