package modules

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/component"
	uservice "gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/modules/user/service"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
