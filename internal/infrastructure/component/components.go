package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/config"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/responder"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/infrastructure/tools/cryptography"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	TokenManager cryptography.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography.Hasher
}

func NewComponents(conf config.AppConf, tokenManager cryptography.TokenManager, responder responder.Responder, decoder godecoder.Decoder, hash cryptography.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, TokenManager: tokenManager, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger}
}
