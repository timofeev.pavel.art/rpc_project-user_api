package user

import (
	"context"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/user"
	pb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/user_grpc"
	"gitlab.com/timofeev.pavel.art/rpc_project-user_api/internal/modules/user/service"
)

type UserServiceGRPC struct {
	userService service.Userer
	pb.UnimplementedUsererServer
}

func NewUserServiceGRPC(userService service.Userer) *UserServiceGRPC {
	return &UserServiceGRPC{userService: userService}
}

func (u *UserServiceGRPC) CreateUser(ctx context.Context, request *pb.UserCreateRequest) (*pb.UserCreateResponse, error) {

	out := u.userService.Create(ctx, user.UserCreateIn{
		Name:     request.GetName(),
		Phone:    request.GetPhone(),
		Email:    request.GetEmail(),
		Password: request.GetPassword(),
		Role:     int(request.GetRole()),
	})

	return &pb.UserCreateResponse{
		UserID:    int64(out.UserID),
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) UdpateUser(ctx context.Context, request *pb.UserUpdateRequest) (*pb.UserUpdateResponse, error) {
	fields := make([]int, len(request.Fields))
	if request.Fields != nil {
		for i, item := range request.Fields {
			fields[i] = int(item)
		}
	}

	out := u.userService.Update(ctx, user.UserUpdateIn{
		User: models.User{
			ID:            int(request.User.GetID()),
			Name:          request.User.GetName(),
			Phone:         request.User.GetPhone(),
			Email:         request.User.GetEmail(),
			Password:      request.User.GetPassword(),
			Role:          int(request.User.GetRole()),
			Verified:      request.User.GetVerified(),
			EmailVerified: request.User.GetEmailVerified(),
			PhoneVerified: request.User.GetPhoneVerified(),
		},
		Fields: fields,
	})

	return &pb.UserUpdateResponse{
		Success:   out.Success,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, request *pb.UserVerifyEmailRequest) (*pb.UserUpdateResponse, error) {

	out := u.userService.VerifyEmail(ctx, user.UserVerifyEmailIn{UserID: int(request.GetUserID())})

	return &pb.UserUpdateResponse{
		Success:   out.Success,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, request *pb.UserPasswordRequest) (*pb.UserPasswordResponse, error) {

	out := u.userService.ChangePassword(ctx, user.ChangePasswordIn{
		UserID:      int(request.GetUserID()),
		OldPassword: request.GetOldPassword(),
		NewPassword: request.GetNewPassword(),
	})

	return &pb.UserPasswordResponse{
		Success:   out.Success,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetUserByEmail(ctx context.Context, request *pb.UserGetByEmailRequest) (*pb.UserResponse, error) {

	out := u.userService.GetByEmail(ctx, user.GetByEmailIn{
		Email: request.Email,
	})

	userPb := pb.User{
		ID:            int64(out.User.ID),
		Name:          out.User.Name,
		Phone:         out.User.Phone,
		Email:         out.User.Email,
		Password:      out.User.Password,
		Role:          int64(out.User.Role),
		Verified:      out.User.Verified,
		EmailVerified: out.User.EmailVerified,
		PhoneVerified: out.User.PhoneVerified,
	}

	return &pb.UserResponse{
		User:      &userPb,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetUserByPhone(ctx context.Context, request *pb.UserGetUserByPhoneRequest) (*pb.UserResponse, error) {

	out := u.userService.GetByPhone(ctx, user.GetByPhoneIn{
		Phone: request.Phone,
	})

	userPb := pb.User{
		ID:            int64(out.User.ID),
		Name:          out.User.Name,
		Phone:         out.User.Phone,
		Email:         out.User.Email,
		Password:      out.User.Password,
		Role:          int64(out.User.Role),
		Verified:      out.User.Verified,
		EmailVerified: out.User.EmailVerified,
		PhoneVerified: out.User.PhoneVerified,
	}

	return &pb.UserResponse{
		User:      &userPb,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetUserByID(ctx context.Context, request *pb.UserGetUserByIDRequest) (*pb.UserResponse, error) {

	out := u.userService.GetByID(ctx, user.GetByIDIn{
		UserID: int(request.UserID),
	})

	userPb := pb.User{
		ID:            int64(out.User.ID),
		Name:          out.User.Name,
		Phone:         out.User.Phone,
		Email:         out.User.Email,
		Password:      out.User.Password,
		Role:          int64(out.User.Role),
		Verified:      out.User.Verified,
		EmailVerified: out.User.EmailVerified,
		PhoneVerified: out.User.PhoneVerified,
	}

	return &pb.UserResponse{
		User:      &userPb,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetUsersByID(ctx context.Context, request *pb.UserGetUsersByIDRequest) (*pb.UsersResponse, error) {
	userIds := make([]int, len(request.UserIDs))
	users := make([]*pb.User, len(request.UserIDs))

	for i, item := range request.UserIDs {
		userIds[i] = int(item)
	}

	out := u.userService.GetByIDs(ctx, user.GetByIDsIn{
		UserIDs: userIds,
	})

	for i, item := range out.User {
		users[i] = &pb.User{
			ID:            int64(item.ID),
			Name:          item.Name,
			Phone:         item.Phone,
			Email:         item.Email,
			Password:      item.Password,
			Role:          int64(item.Role),
			Verified:      item.Verified,
			EmailVerified: item.EmailVerified,
			PhoneVerified: item.PhoneVerified,
		}
	}

	return &pb.UsersResponse{
		User:      users,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}
